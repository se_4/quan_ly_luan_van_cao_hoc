# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table faculty (
  id                        bigint not null,
  mgv                       varchar(255),
  name                      varchar(255),
  degree                    varchar(255),
  place_work                varchar(255),
  phone_number              varchar(255),
  email                     varchar(255),
  constraint pk_faculty primary key (id))
;

create table student (
  id                        bigint not null,
  faculty_id                bigint,
  msv                       varchar(255),
  name                      varchar(255),
  birthday                  timestamp,
  address                   varchar(255),
  phone_number              varchar(255),
  sectors                   varchar(255),
  courses                   varchar(255),
  email                     varchar(255),
  thesis                    varchar(255),
  constraint pk_student primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  student_id                bigint,
  faculty_id                bigint,
  constraint pk_user_account primary key (id))
;

create sequence faculty_seq;

create sequence student_seq;

create sequence user_account_seq;

alter table student add constraint fk_student_faculty_1 foreign key (faculty_id) references faculty (id);
create index ix_student_faculty_1 on student (faculty_id);
alter table user_account add constraint fk_user_account_student_2 foreign key (student_id) references student (id);
create index ix_user_account_student_2 on user_account (student_id);
alter table user_account add constraint fk_user_account_faculty_3 foreign key (faculty_id) references faculty (id);
create index ix_user_account_faculty_3 on user_account (faculty_id);



# --- !Downs

drop table if exists faculty cascade;

drop table if exists student cascade;

drop table if exists user_account cascade;

drop sequence if exists faculty_seq;

drop sequence if exists student_seq;

drop sequence if exists user_account_seq;

