package models;

import java.lang.String;
import java.util.Date;
import play.data.format.Formats;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;
import com.avaje.ebean.Page;

import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import models.*;

import java.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import javax.validation.*;
import javax.validation.metadata.*;
import com.avaje.ebean.*;

import play.libs.F;

@Entity
public class Faculty extends Model implements PathBindable<Faculty>
{
    @Id
    public Long id;

    @OneToOne (mappedBy = "faculty")
    public UserAccount userAccount;

    @OneToMany(mappedBy = "faculty")
    public List<Student> students;

    @Constraints.Required
    public String mgv;

    @Constraints.Required
    public String name;

	public String degree;
    
    public String placeWork;
	
    public String phoneNumber;

    //dinh dang email
	@Target({FIELD})
	@Retention(RUNTIME)
	@Constraint(validatedBy = EmailValidator.class)
	@play.data.Form.Display(name="VD: example@vnu.edu.vn", attributes={"value"})
	public static @interface EMAIL {
		String message() default EmailValidator.message;
		Class<?>[] groups() default {};
		Class<? extends Payload>[] payload() default {};
	}

	public static class EmailValidator extends Constraints.Validator<String> implements ConstraintValidator<EMAIL, String> {
		final static public String message = "Nhập lại";
        
		public EmailValidator() {}

		@Override
		public void initialize(EMAIL constraintAnnotation) {}
    
		@Override
		public boolean isValid(String value) {
			String pattern = "^[_A-Za-z0-9-\\+]*@"
            + "vnu.edu.vn$";
			return value != null && value.matches(pattern);
		}

		@Override
		public F.Tuple<String, Object[]> getErrorMessageKey() {
			return new F.Tuple<String, Object[]>(message,
			new Object[]{});
		}
	}
	@EMAIL
    public String email;

    public static Finder<Long,Faculty> find = new Finder<Long,Faculty>(Long.class, Faculty.class);

    /*public Faculty(){}
    public Faculty(String mgv, String name, String placeWork, String phoneNumber) {
        this.mgv = mgv;
        this.name = name;
        this.placeWork = placeWork;
        this.phoneNumber = phoneNumber;
    }
*/
    public String toString(){
        return String.format("%s - %s", mgv, name);
    }

    public static List<Faculty> findAll(){
        return find.all();
    }

    public static Page<Faculty> find(int page) {//tra ve trang thay vi list
        return find.where()
                .orderBy("id asc")//sap sep tang dan theo id
                .findPagingList(5)//quy dinh kich thuoc cua trang
                .setFetchAhead(false)//co can lay du lieu mot the
                .getPage(page);//lay trang hien tai, bat dau tu trang 0
    }

    public static Faculty findByMgv(String mgv){
        return find.where().eq("mgv",mgv).findUnique();
    }
	public static Faculty findById(Long id){
        return find.where().eq("id",id).findUnique();
    }

    public static Faculty findByEmail(String email){
        return find.where().eq("email",email).findUnique();
    }

    public static List<Faculty> findByName(String term){
        final List<Faculty> results = new ArrayList<Faculty>();
        for(Faculty candidate : Faculty.findAll()) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }
	 
	// public static Map<String,String> options() {
        // LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
        // for(Faculty c: Faculty.find.orderBy("name").findList()) {
            // options.put(c.id.toString(), c.name);
        // }
        // return options;
    // }

    @Override
    public Faculty bind(String key, String value){
        return findByMgv(value);
    }

    @Override
    public String unbind(String key){
        return mgv;
    }

    @Override
    public String javascriptUnbind(){
        return mgv;
    }
}