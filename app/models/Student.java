package models;

import java.lang.String;
import java.util.Date;
import play.data.format.Formats;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;
import com.avaje.ebean.Page;

import play.mvc.PathBindable;
import play.mvc.QueryStringBindable;

import models.*;

import java.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
import javax.validation.*;
import javax.validation.metadata.*;
import com.avaje.ebean.*;

import play.libs.F;

@Entity
public class Student extends Model implements PathBindable<Student>
{
	@Id
	public Long id;

    @OneToOne (mappedBy = "student")
    public UserAccount userAccount;

    @ManyToOne
    public Faculty faculty;
	
	//dinh dang ma sinh vien	
	@Target({FIELD})
	@Retention(RUNTIME)
	@Constraint(validatedBy = MsvValidator.class)
	@play.data.Form.Display(name="gồm 8 chữ số", attributes={"value"})
	public static @interface MSV {
		String message() default MsvValidator.message;
		Class<?>[] groups() default {};
		Class<? extends Payload>[] payload() default {};
	}

	public static class MsvValidator extends Constraints.Validator<String> implements ConstraintValidator<MSV, String> {
		final static public String message = "nhập lại";
        
		public MsvValidator() {}

		@Override
		public void initialize(MSV constraintAnnotation) {}
    
		@Override
		public boolean isValid(String value) {
			String pattern = "^[0-9]{8}$";
			return value != null && value.matches(pattern);
		}

		@Override
		public F.Tuple<String, Object[]> getErrorMessageKey() {
			return new F.Tuple<String, Object[]>(message,
			new Object[]{});
		}
	}
	@MSV
	public String msv;
	
    @Constraints.Required
    public String name;

    @Formats.DateTime(pattern = "dd-MM-yyyy")
    public Date birthday;

    public String address;
	
    public String phoneNumber;

    public String sectors;

    public String courses;

	//dinh dang email
	@Target({FIELD})
	@Retention(RUNTIME)
	@Constraint(validatedBy = EmailValidator.class)
	@play.data.Form.Display(name="VD: example@vnu.edu.vn", attributes={"value"})
	public static @interface EMAIL {
		String message() default EmailValidator.message;
		Class<?>[] groups() default {};
		Class<? extends Payload>[] payload() default {};
	}

	public static class EmailValidator extends Constraints.Validator<String> implements ConstraintValidator<EMAIL, String> {
		final static public String message = "Nhập lại";
        
		public EmailValidator() {}

		@Override
		public void initialize(EMAIL constraintAnnotation) {}
    
		@Override
		public boolean isValid(String value) {
			String pattern = "^[_A-Za-z0-9-\\+]*@"
            + "vnu.edu.vn$";
			return value != null && value.matches(pattern);
		}

		@Override
		public F.Tuple<String, Object[]> getErrorMessageKey() {
			return new F.Tuple<String, Object[]>(message,
			new Object[]{});
		}
	}
	@EMAIL
    public String email;

    public String thesis;

	public static Finder<Long,Student> find = new Finder<Long,Student>(Long.class, Student.class);
    
    public Student(){}
    public Student(String msv, Date birthday, String name, String email){
		this.msv = msv;
        this.name = name;
        this.email = email;
        this.birthday = birthday;

        /*SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
        this.birthday = String.valueOf(format.format(birthday));*/
    }

    public String toString(){
        return String.format("%s - %s - %s",msv, name, email);
    }

    public static List<Student> findAll(){
        return find.all();
    }
	
	public static Page<Student> find(int page) {//tra ve trang thay vi list
		return find.where()
					.orderBy("id asc")//sap sep tang dan theo id
					.findPagingList(10)//quy dinh kich thuoc cua trang
					.setFetchAhead(false)//co can lay du lieu mot the
					.getPage(page);//lay trang hien tai, bat dau tu trang 0
	}
	
    public static Student findByMsv(String msv){
        return find.where().eq("msv",msv).findUnique();
	}
	
	public static Student findById(Long id){
        return find.where().eq("id",id).findUnique();
	}

    public static Student findByEmail(String email){
        return find.where().eq("email",email).findUnique();
    }

    public static List<Student> findByName(String term){
        final List<Student> results = new ArrayList<Student>();
        for(Student candidate : Student.findAll()) {
            if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
                results.add(candidate);
            }
        }
        return results;
    }

	
	@Override
	public Student bind(String key, String value){
		return findByMsv(value);
	}
	
	@Override
	public String unbind(String key){
		return msv;
	}
	
	@Override
	public String javascriptUnbind(){
		return msv;
	}
}