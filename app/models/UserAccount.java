package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;
//import play.db.ebean.Ebean;
import javax.persistence.*;
import models.Student;
import models.Faculty;

@Entity
public class UserAccount extends Model{
	@Id
	public Long id;
	@Constraints.Required
	public String email;
	@Constraints.Required
	public String password;

	@OneToOne
	public Student student;

	@OneToOne
	public Faculty faculty;

	public UserAccount (){}
	public UserAccount(String email, String password){
		this.email = email;
		this.password = password;
	}
	
	//hoc vien da duoc giao vu tao tai khoan cho
	//hoc vien dang nhap bang mail truong va ma sinh vien cua minh
	public static UserAccount findByEmail(String email){
		return finder.where().eq("email", email).findUnique();
	}
	
	public static UserAccount findByStudentId(Long student_id){
		return finder.where().eq("student_id", student_id).findUnique();
	}
	
	public static UserAccount findByFacultyId(Long faculty_id){
		return finder.where().eq("faculty_id", faculty_id).findUnique();
	}
	
	public static UserAccount authenticate(String email, String password){
		return finder.where().eq("email", email)
							 .eq("password", password).findUnique();
	}
	
	public void setUserAccount(String email,String password){
		this.email = email;
		this.password = password;
	}
	
	public static Finder<Long,UserAccount> finder = 
				new Finder<Long,UserAccount>(Long.class,UserAccount.class);
}