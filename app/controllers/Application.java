package controllers;

import play.*;
import play.mvc.*;
import play.data.Form;
import models.*;
import views.html.*;
import static play.data.Form.form;

public class Application extends Controller {

	private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result index() {
        return ok(index.render());
    }

	public static class Login{
		public String email;
		public String password;
	}
	public static Result login(){
		return ok(login.render(form(Login.class)));
	}

	public static Result authenticate(){
		Form<Login> loginForm = form(Login.class).bindFromRequest();
		String email = loginForm.get().email;
		String password = loginForm.get().password;
		
		session().clear(); //don dep session de chuan bi cho cac thong tin dang nhap moi
		UserAccount userAccount = UserAccount.authenticate(email, password);
		if(userAccount == null){
			flash("error", "Email hoặc mật khẩu không đúng");
			return redirect(routes.Application.login());
		}
		session("email",email);
		Student student = Student.findByEmail(email);
		if(email.equals("giaovu@vnu.edu.vn")){
			return redirect(routes.Students.list(0));
		}else if(student != null){
			return redirect(routes.Students.information(Student.findByEmail(email)));
		}else 
			return redirect(routes.Faculties.details(Faculty.findByEmail(email)));
	}
	public static Result profile(){
		String email = session().get("email");
		Student student = Student.findByEmail(email);
		if(email.equals("giaovu@vnu.edu.vn")){
			return redirect(routes.Students.list(0));
		}else if(student != null){
			return redirect(routes.Students.information(Student.findByEmail(email)));
		}else 
			return redirect(routes.Faculties.details(Faculty.findByEmail(email)));
	}
	public static Result logout(){
		session().clear();
		return redirect(routes.Application.login());
	}
}
