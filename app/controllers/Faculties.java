package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import java.util.List;
import models.*;
import play.data.Form;
//import java.lang.String;
//import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.mvc.Security;
import models.UserAccount;

@Security.Authenticated(Secured.class)
public class Faculties extends Controller{
    private static final Form<Faculty> facultyForm = Form.form(Faculty.class);

    public static Result list(Integer page){
        Page<Faculty> faculties = Faculty.find(page);
		String email = session().get("email");
		Student student = Student.findByEmail(email);
		Faculty faculty = Faculty.findByEmail(email);
		if(email.equals("giaovu@vnu.edu.vn")){
			return ok(views.html.staff.listFaculty.render(faculties));
		}else if(student != null){
			return ok(views.html.students.listFaculty.render(faculties));
		}else 
			return ok(views.html.faculties.listFaculty.render(faculties));
    }

    public static Result newFaculty(){
        return ok(views.html.staff.detailFaculty.render(facultyForm));
    }

    public static Result details(Faculty faculty){
        if(faculty == null){
            return notFound(String.format("Faculty does not exist."));
        }
        Form<Faculty> filledForm = facultyForm.fill(faculty);
		if(session().get("email").equals("giaovu@vnu.edu.vn"))
        return ok(views.html.staff.detailFaculty.render(filledForm));
		else return ok(views.html.faculties.detailFaculty.render(filledForm));
		
    }
	
	public static Result info(Faculty faculty){
		if(faculty ==null){
			return notFound(String.format("Faculty dose not exist."));
		}
		return ok(views.html.students.info.render(faculty));
	}
	
    public static Result save(){
        Form<Faculty> boundForm = facultyForm.bindFromRequest();
        if(boundForm.hasErrors()){
            flash("error","Hoàn thành theo đúng form quy định.");
			if(session().get("email").equals("giaovu@vnu.edu.vn"))
            return badRequest(views.html.staff.detailFaculty.render(boundForm));
			else badRequest(views.html.faculties.detailFaculty.render(boundForm));
        }
        Faculty faculty = boundForm.get();

        
		if(faculty.id == null){
			if(Faculty.findByMgv(faculty.mgv)!= null){
				flash("error","Mã giảng viên này đã tồn tại.");
				return badRequest(views.html.staff.detailFaculty.render(boundForm));
			}
			if(Faculty.findByEmail(faculty.email)!= null || Student.findByEmail(faculty.email) != null){
				flash("error","Email này đã tồn tại.");
				return badRequest(views.html.staff.detailFaculty.render(boundForm));
			}
			faculty.save();
			UserAccount userAccount = new UserAccount(faculty.email,faculty.mgv);
			userAccount.faculty = faculty;
			userAccount.save();
		}else{
			Faculty faculty1 = Faculty.findById(faculty.id);
			if(Faculty.findByMgv(faculty.mgv)!= null && !faculty.mgv.equals(faculty1.mgv)){
				flash("error","Mã giảng viên này đã tồn tại.");
				return badRequest(views.html.staff.detailFaculty.render(boundForm));
			}
			if(Faculty.findByEmail(faculty.email)!= null || Student.findByEmail(faculty.email) != null){
				if(!faculty.email.equals(faculty1.email)){
					flash("error","Email này đã tồn tại.");
					return badRequest(views.html.staff.detailFaculty.render(boundForm));
				}
			}
			faculty.update();
			UserAccount userAccount = UserAccount.findByFacultyId(faculty.id);
			userAccount.setUserAccount(faculty.email,faculty.mgv);
			userAccount.update();
		}
        flash("success", String.format("Lưu thành công gaio viên %s",faculty));
        return redirect(routes.Faculties.list(0));
    }

    public static Result delete(String mgv){
        final Faculty faculty = Faculty.findByMgv(mgv);
        if(faculty == null){
            return notFound(String.format("Student %s does not exist.",faculty));
        }
        
		UserAccount userAccount = UserAccount.findByFacultyId(faculty.id);
		userAccount.delete();
		faculty.delete();
        return redirect(routes.Faculties.list(0));
    }

}