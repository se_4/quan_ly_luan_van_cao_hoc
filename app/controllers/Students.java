package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import java.util.List;
import models.Student;
import play.data.Form;
import java.lang.String;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import play.mvc.Security;
import models.*;


@Security.Authenticated(Secured.class)
public class Students extends Controller{

    private static final Form<Student> studentForm = Form.form(Student.class);

    public static Result list(Integer page){
        Page<Student> students = Student.find(page);
		String email = session().get("email");
		Student student = Student.findByEmail(email);
		Faculty faculty = Faculty.findByEmail(email);
		if(email.equals("giaovu@vnu.edu.vn")){
			return ok(views.html.staff.listStudent.render(students));
		}else if(student != null){
			return ok(views.html.students.listStudent.render(students));
		}else 
			return ok(views.html.faculties.listStudent.render(students));
    }

    public static Result newAccount(){
        return ok(views.html.staff.detailStudent.render(studentForm));
    }

    public static Result information(Student student){
        if(student == null){
            return notFound(String.format("Student does not exist."));
        }
        Form<Student> filledForm = studentForm.fill(student);
		if(session().get("email").equals("giaovu@vnu.edu.vn")){
			return ok(views.html.staff.detailStudent.render(filledForm));
		}else
			return ok(views.html.students.detailStudent.render(filledForm));
    }

    public static Result save(){
        Form<Student> boundForm = studentForm.bindFromRequest();
        if(boundForm.hasErrors()){
            flash("error","Hoàn thành theo form quy định.");
			if(session().get("email").equals("giaovu@vnu.edu.vn"))
            return badRequest(views.html.staff.detailStudent.render(boundForm));
			else return badRequest(views.html.students.detailStudent.render(boundForm));
        }
        Student student = boundForm.get();
		

		if(student.id == null){
			if(Student.findByMsv(student.msv)!= null){
				flash("error","Mã học viên này đã tồn tại.");
				return badRequest(views.html.staff.detailStudent.render(boundForm));
			}
			if(Student.findByEmail(student.email)!= null || Faculty.findByEmail(student.email)!= null){
				flash("error","Email này đã tồn tại.");
				return badRequest(views.html.staff.detailStudent.render(boundForm));
			}
			student.save();
			UserAccount userAccount = new UserAccount(student.email,student.msv);
			userAccount.student = student;
			userAccount.save();
		}else{
			Student student1 = Student.findById(student.id);
			if(Student.findByMsv(student.msv)!= null && !student.msv.equals(student1.msv)){
				flash("error","Mã học viên này đã tồn tại.");
				return badRequest(views.html.staff.detailStudent.render(boundForm));
			}
			if(Student.findByEmail(student.email)!= null || Faculty.findByEmail(student.email)!= null){
				if(!student.email.equals(student1.email)){
					flash("error","Email này đã tồn tại.");
					return badRequest(views.html.staff.detailStudent.render(boundForm));
				}
			}
			student.update();
			UserAccount userAccount = UserAccount.findByStudentId(student.id);
			userAccount.setUserAccount(student.email,student.msv);
			userAccount.update();
		}
		
        flash("success", String.format("Lưu thành công %s",student));
        return redirect(routes.Students.list(0));
    }

    public static Result delete(String msv){
        final Student student = Student.findByMsv(msv);
        if(student == null){
            return notFound(String.format("Student %s does not exist.",student));
        }
		UserAccount userAccount = UserAccount.findByStudentId(student.id);
		userAccount.delete();
		student.delete();
        return redirect(routes.Students.list(0));
    }
	
}